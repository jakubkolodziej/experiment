\documentclass[a4paper,12pt]{article}
\usepackage[left=2cm, right=2cm, top=2cm,bottom=2cm]{geometry}
\usepackage{amsmath,amsfonts,amsthm}
\usepackage{enumitem}
\usepackage{sistyle}
\usepackage{graphicx}
\usepackage{indentfirst}

\title{%
  STAT0029 Assessment 1 \\
  \large Evaluation of the pocket-sized spectrometer  performance in \\
  detecting nutritional values of sweetened drinks}

\author{Student numbers: 18046293, 18130769, 18056729}
\date{\today}

\renewcommand{\thesection}{\arabic{section}.}
\renewcommand \thesubsection {(\roman{subsection})}

\begin{document}
\maketitle

\begin{abstract}
In our experiment we test if a Do-It-Yourself pocket-sized spectrometer could be used to detect variability in sugar and sweetener content in coloured drinks. Our results show significant differences in light intensity detected by the device for solutions with different concentration of sugar and sweetener. However, our spectrometer was not precise enough to differentiate between pairs with low and high levels of concentration. This fact and other practical considerations indicate a limited usefulness of our spectrometer outside of the laboratory setting. 
\end{abstract}

\section{Introduction}
\par \noindent
Professional spectrometers costing thousands of dollars are increasingly used in the food industry to keep products parameters stable over time\footnote{Lu H., Zhang H, Chingin K., Xiong J., Fang X, Chen H., Ambient mass spectrometry for food science and industry, TrAC Trends in Analytical Chemistry 2018 v.107, pp. 99-115.}. 
Our experiment aims at assessing if a simpler and less expensive hand-held device could be potentially used in the future by consumers to measure nutritional values of foods and drinks. We were inspired by a prototype SCiO\textsuperscript{\texttrademark} Molecular Sensor built by Consumer Physics\footnote{Consumer Physics, SCiO - The World's First Pocket Sized Molecular Sensor. Available at: https://www.consumerphysics.com/scio-for-consumers/. Accessed on 23rd February 2020.}.
We use a Do-It-Yourself (DIY) spectrometer available from Public Lab\footnote{Public Lab, Lego Spectrometer Kit, Available at: https://publiclab.org/wiki/lego-spectrometer, Accessed on 23rd February 2020.} for approximately £40. In our experiment we analyse if this spectrometer can successfully detect sugar and sweetener content variations in different coloured drinks. 

\section{Design and data}
\par \noindent
We start by providing a short explanation of how spectrometry works so that readers less familiar with this technique can better understand our experiment design.
\medskip
\par \noindent
A spectrometer measures the absorption of light by substances at different wavelengths. As light passes through a substance parts of the light spectrum are absorbed to a different extent. A diffraction grating located behind the sample disperses the light by wavelength. A detector measures the intensity of light at different colours that correspond to different wavelengths. A simple diagram of the spectrometer is presented in Figure 1. The function of the slit shown on the diagram is to control the amount and angle of light entering the spectrometer.    
\medskip \par \noindent 
In our device we use a piece of a CD as a diffraction grating and a 8MP web camera as a detector. Use of a standard RGB (Red Green Blue) camera limits the wavelength range that we can measure to approximately visible spectrum. We capture spectra using a web-based software Spectral Workbench\footnote{Public Lab, Spectral Workbench, Available at: https://spectralworkbench.org/dashboard, Accessed on 23rd February 2020.}. As the camera measures the light intensity in the RGB channels for pixels across the spectrum we average measurements across all channels and scale them to be between 0\% and 100\%. 
\medskip \par \noindent 
As we are interested in obtaining light intensity readings by wavelength, we need to map pixels to wavelength ranges in the process of calibration. To calibrate our spectrometer we use a standard 23W compact fluorescent lamp (CFL) and match our spectral reading for its light to the provided  benchmark\footnote{Public Lab, Spectral Workbench calibration, Available at: https://publiclab.org/wiki/spectral-workbench-calibration, Accessed on 23rd February 2020.}. As CFL emits light only in narrow wavelength ranges that are well known, we can align pixels to these ranges and recover the full pixel to nm wavelength mapping.  
\medskip \par \noindent
The captured spectra for our treatments are represented by graphs that depict the intensity at each wavelength compared to the intensity of the source light. This absorption pattern can be seen as a fingerprint for that substance. Note that adding colours to the solution will have a direct influence on this fingerprint since the colour we observe is due to the the light spectrum that is not absorbed by the solution. For instance, a yellow solution absorbs blue wavelengths, but not yellow ones.
\begin{figure}[htp]
    \centering
    \includegraphics[width=10cm]{Diagram2.png}
    \caption{Spectrometer diagram\protect\footnotemark}
    \label{fig:diagram}
\end{figure}
\footnotetext{verticity (username), An inexpensive spectrometer for doing acetone testing on kava. Available at: http://kavaforums.com/forum/threads/an-inexpensive-spectrometer-for-doing-acetone-testing-on-kava.3880/. Accessed on 23rd February 2020.}
\par \noindent 
The goal of our experiment is to test if our DIY spectrometer is accurate enough to capture differences in wavelength intensity across the visible light spectrum between solutions varying in sugar and sweetener concentrations. This should allow a device user to approximate the intake of calories from consuming sweetened drinks.
\medskip \par \noindent 
We consider five \textbf{treatments}:
\begin{itemize}[itemsep=1.15pt,topsep=1.15pt]
\item water,
\item \emph{low sugar} - water with approx. 0.08g/100ml baking sugar concentration,
\item \emph{high sugar} - water with approx. 0.16g/100ml baking sugar concentration,
\item \emph{low sweetener} - water with approx. 0.08g/100ml sweetener concentration,
\item \emph{high sweetener} - water with approx. 0.16g/100ml sweetener concentration.
\end{itemize}
\medskip \par \noindent 
Low and high levels of sugar roughly match the sugar content found in moderately and very sweet sodas sold in the UK. To give a reference Coca-Cola contains around 10.6g sugar/100ml\footnote{Parkinson J., Sugar tax: What is the UK's most sugary drink?, Available at: https://www.bbc.co.uk/news/magazine-35831125, Accessed on 23rd February 2020.}.
\medskip \par \noindent 
Testing both baking sugar and a sweetener allows us to extend results to different types of sweetened drinks.  Although it is virtually impossible to distinguish one from another by taste, they are chemically different and have different nutritional values. We use Canderel (maltodextrin), as a sweetener, because it has a comparable level of sweetness per unit of volume as sugar. This makes obtaining solutions with comparable levels of sweetness much easier.
\medskip \par \noindent 
Most sodas include artificial colouring. As a solution hue can directly influence the response variable, we design \textbf{blocks} of five different colours (none, yellow, green, red and black). These tints are obtained by adding a drop of Ktdorns food colouring into each solution. We made sure that these dyes are sugar and sweetener-free so that they do not interfere with the treatments. In Figure 2 we show the solutions prepared for testing.
\begin{figure}[htp]
    \centering
    \includegraphics[width=10cm]{Glasses.jpeg}
    \caption{Solutions prepared for testing}
    \label{fig:solutions}
\end{figure}
\medskip \par \noindent 
We expected that a light source can also impact measured intensities. Trial runs turned out be very instructive in this regard. We initially considered using a halogen bulb, but it was generating too much heat and melting the tape used in our set-up. We decided to stick to LEDs. We noticed that some LEDs emit diffused light that led to results not on par with those that could be obtained from a similar bulb with a directed beam (\emph{reflector}). Therefore we created two \textbf{blocks} to underline this effect. We use two 9W LEDs for our measurements -one \emph{directed} from Paul Russells and one \emph{diffused} from Tesco. 
\medskip \par \noindent 
Our selection of blocking variables is supported by academic research showing that both colour and light source can have a significant impact on light intensity\footnote{Vanderveen J.R., Martin B., Ooms K.J., Developing Tools for Undergraduate Spectroscopy: An Inexpensive Visible Light Spectrometer, J. Chem. Educ. 2013, 90, 7, pp. 894-899.}.
\medskip \par \noindent 
Extra care was taken during the preparation of the experiment. The spectrometer was carefully calibrated and a distance between a bulb and the device was selected to get an optimal exposure for testing. Containers and spoons used to measure and stir solutions were thoroughly cleaned between preparations and a new pipette was used for each sample.
\medskip \par \noindent 
Even though our spectrometer was well isolated, we \textbf{randomised} the order in which data was collected to account for fluctuations in internal factors (fluctuating LED intensity) and external ones (temperature, light intensity outside) which could be potentially confounding. 

\begin{figure}[htp]
    \centering
    \includegraphics[width=6cm]{Setup.jpeg}
    \caption{Our DIY spectrometer}
    \label{fig:spectrometer}
\end{figure}
\medskip \par \noindent 
We took 20 measurements for each combination of the light source, colour and treatment (total of 1,000). Light intensity readings were averaged in 10nm wavelength groups (around 27 pixels) to reduce the measurement error.

\section{Analysis}
\subsection{ANOVA approach}
\par \noindent 
We test whether our spectrometer could capture sugar and sweetener saturation differences, as measured by light intensity. Our research hypothesis is: 
\medskip \par \noindent 
$H_{0}$: All treatment effects are equal $\alpha_{1}=\alpha_{2}=\alpha_{3}=\alpha_{4}=\alpha_{5}$ \\
$H_{1}$: At least one treatment effect is different
\medskip \par \noindent 
We start our analysis by applying the analysis of variance (ANOVA) with light intensity as a response, different sugar and sweetener concentrations as a treatment variable and dye colours, wavelength range groups and LED types as blocking variables. Academic evidence from another domain suggests that ANOVA can be used to analyse solution saturation levels based on spectral measurements\footnote{Li W., Lin L., Li G., Wavelength selection method based on test analysis of variance: application to oximetry. Analytical Methods, Issue 2014, 6, pp. 1082-1089.}.
\medskip \par \noindent 
Our ANOVA model equation is: 
$$Y_{ijklm}=\mu+\alpha_{j}+\beta_{k}+\gamma_{l}+\delta_{m}+\epsilon_{ijklm}$$
with $i=1,...,n_{i}$, $j=1,...,n_{j}$, $k=1,...,n_{k}$, $l=1,...,n_{l}$, $m=1,...,n_{m}$;
where $n_{i}=20$ is the number of measurements taken per combination of treatment, dye color, light type and wavelength group; $n_{j}=5$ is the number of treatments; $n_{k}=5$ is the number of colours; $n_{k}=24$ is the number of wavelength groups (from 420nm to 650nm in 10nm increments); $n_{m}=2$ is the number of light types.\\
Our model uses the sum-to-zero constraints (with all groups having equal size):
$\sum_{j}\alpha_{j}=0$; $\sum_{k}\beta_{k}=0$; $\sum_{l}\gamma_{l}=0$;  $\sum_{m}\delta_{m}=0$.
We assume a significance level of $0.05$. 
\medskip \par \noindent 
We use logs of measurements as our response $Y_{ijklm}$, because according to the Beer-Lambert law absorbance is proportional to to the log of transmittance, which is a proportion of the light that entered to the light that exited the sample (our readings are of transmittance)\footnote{Edinburgh Instruments Ltd, The Beer-Lambert Law, Available at: https://www.edinst.com/blog/the-beer-lambert-law/, Accessed on 23rd February 2020.}. For example if the light passes through a solution without any absorption, then absorbance is zero, and transmittance is 100\%. 
\medskip \par \noindent 
The ANOVA table for our model is included below.
\medskip \par \noindent 
\begin{center}
\begin{tabular}{|l|r|r|r|r|}
\hline
Source of variation & df & m.s.s.  & m.s.r. & p-value\\
\hline
Treatment & 4 & 0.5 & 6.5 & 3.3e-05\\
Colour & 4 & 62.0 & 765.1 & $<$2e-16\\
Wavelength group & 23 & 157.0 & 1937.1 & $<$2e-16\\
Light type & 1 & 2290.7 & 28268.6 & $<$2e-16\\
Residuals & 23967 & 0.1 &  & \\
\hline
\end{tabular}
\end{center}
\medskip \par \noindent 
While treatment effect is statistically significant, we noticed that due to the low light intensity generated by the \emph{diffused} LED, our results were driven solely by differences in log transmittance for the \emph{reflector}. As differences for the \emph{diffused} light were below detection levels we decided to exclude it from the experiment, therefore removing light type as a blocking variable ($\delta_{m}$ term) and simplifying our model to:
$$Y_{ijkl}=\mu+\alpha_{j}+\beta_{k}+\gamma_{l}+\epsilon_{ijkl}$$
Results for a new model are provided below. 
\begin{center}
\begin{tabular}{|l|r|r|r|r|}
\hline
Source of variation & df & m.s.s.  & m.s.r. & p-value\\
\hline
Treatment & 4 & 0.3 & 11.3 & 4.1e-09\\
Colour & 4 & 24.8 & 804.3 & $<$2e-16\\
Wavelength group & 23 & 133.6 & 4326.0 & $<$2e-16\\
Residuals & 11968 & 0.0 &  & \\
\hline
\end{tabular}
\end{center}
Treatment and both blocking variables (colour and wavelength group) are highly statistically significant. However, for our device to be useful we need it to be able to distinguish between concentrations of sugar and sweetener. We run the Tukey's honest significance test for all pairs of treatments.
\begin{center}
\begin{tabular}{|l|r|r|r|r|}
\hline
Pair & Estimate & LB of 95\% CI & UB of 95\% CI & Adj. p-value\\
\hline
high sweetener - high sugar & -0.026 & -0.039 & -0.012 & 0.000\\
low sweetener - high sugar & -0.022 & -0.036 & -0.008 & 0.000\\
water - high sweetener & 0.024 & 0.010 & 0.038 & 0.000\\
water - low sweetener & 0.021 & 0.007 & 0.034 & 0.000\\
low sugar - high sugar & -0.019 & -0.033 & -0.006 & 0.001\\
water - low sugar & 0.018 & 0.004 & 0.032 & 0.004\\
low sugar - high sweetener & 0.006 & -0.008 & 0.020 & 0.759\\
low sweetener - high sweetener & 0.003 & -0.011 & 0.017 & 0.967\\
low sweetener - low sugar & -0.003 & -0.017 & 0.011 & 0.983\\
water - high sugar & -0.002 & -0.015 & 0.012 & 0.998\\
\hline
\end{tabular}
\end{center}
The results look promising with six out of ten pairs to be statistically significant at $\alpha=0.05$. This means that there is at least one 10nm range in wavelength band that can be used to distinguish between these solution pairs. However, the usefulness of the device would be limited as it seems to be unable to differentiate between high and low levels of sugar and sweetener concentrations. 

\subsection{Testing ANOVA assumptions}
\par \noindent
We validate our model by testing the assumptions of ANOVA.
\medskip \par \noindent 
\textbf{Homogeneity of variance}
\medskip \par \noindent 
To test for inequality of the within-group variances, we conduct a robust variance test - Levene's test for homogeneity of variance. The result of the test are shown below.
\begin{center}
\begin{tabular}{|r|r|r|}
\hline
Test statistic & p-value & df\\
\hline
9.846 & 2.2e-16 & 599\\
\hline
\end{tabular}
\end{center}
At significance level of $\alpha=0.05$ we reject the null hypothesis of the Levene's test that group variances are equal. Nevetheless the ANOVA F-test should be fairly robust against inequality of variances with our group sizes being equal. We can expect that the probability of the Type I error is higher than the assumed 5\%, but our test p-values are a few orders of magnitude lower than 5\%.
\medskip \par \noindent 
\textbf{Independence of samples}
\medskip \par \noindent 
We can assume that our readings are independent, because each sample was placed in a new disposable plastic 4.5ml polystryrene cuvette before readings were taken and order in which measurements were made was randomized.
\medskip \par \noindent 

\medskip \par \noindent 
\textbf{Normality of residuals}
\medskip \par \noindent 
To test if residuals $\epsilon_{ijkl}$ are normally distributed, we plot them on a normal Q-Q plot and histogram. The data on the plot deviates substantially from the straight line, indicating that it did not follow a normal distribution (see Figure 4). It seems that the distribution of residuals is skewed with a fat left tail. This suggests that there are multiple observations with very low light intensity. Once again, the results of F-test should not be affected seriously due to the equal group sizes, but the Type I error will be understated by the significance level. 
\begin{figure}[htp]
    \centering
    \includegraphics[width=14cm]{residual2.png}
    \caption{Residual Plots}
    \label{fig: Residuals plots}
\end{figure}
\medskip \par \noindent 
As we find two departures from the ANOVA assumptions in our experiment, we decided to explore also a non-parametric approach.
\medskip \par \noindent 
\subsection{Non-parametric analysis of variance}
\medskip \par \noindent 
There are two common non-parametric analysis of variance tests - Kruskal-Wallis and Friedman tests. The Friedman Test is more appropriate for our experiment as it is intended for a randomized block design. The Friedman test is often used when the normality or equality of variance assumptions are not satisfied in the parametric ANOVA framework\footnote{Riffenburgh R., Friedman Test, Statistics in Medicine 2nd Edition, 2006.}. The Friedman test hypotheses are the same as in ANOVA F-test. The table below show the results for the Friedman rank sum test.
\begin{center}
\begin{tabular}{ |c|c|c|} 
\hline
Test statistic & p-value & df \\ 
\hline
41.939 & 1.717e-08 & 4 \\
\hline
\end{tabular}
\end{center}
As the table above shows the test p-value of 1.717e-08. We can reject the null hypothesis that the treatment means are equal. This result reaffirms the results obtained from the ANOVA F-test. 
\medskip \par \noindent 
It bears to mention that while the Friedman test does not necessitate for the data to be normally distributed, it does require the data to be independent and for the group variance to be approximately equal\footnote{Smalheiser, N., Data Literacy. How to make your experiments robust and reproducible, 2017, pp. 162. Available at: https://doi.org/10.1016/B978-0-12-811306-6.00012-9}.


\section{Discussion}
\medskip \par \noindent 
While our results presented above show statistically significant results, we believe that the practical usefulness of this DIY spectrometer in detecting differences in sugar and sweetener content is limited due to:
\begin{itemize}[itemsep=1.15pt,topsep=1.15pt]
\item the fact that the device was not precise enough to differentiate between pairs with low and high levels of concentration.
\item sensitivity of measurements to the conditions. It would be impossible to ascertain the right conditions (light intensity, camera angle etc.) outside of the laboratory setting.
\item the fact that as we discovered in our trial runs, common drinks (wine, milk) are too dense to let enough light though to allow measurements.
\end{itemize}
\medskip \par \noindent 
\textbf{Word count:} 1991

\end{document}

