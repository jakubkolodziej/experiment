---
title: "STAT0029 Experiment"
output:
  html_notebook:
    code_folding: hide
    toc: true
    toc_depth: 2
    toc_float:
      collapsed: true
      smooth_scroll: true
---


```{r setup, include = FALSE}
library(jsonlite)
library(tidyverse)
library(broom)
library(purrr)
library(httr)
library(glue)
library(rvest)
library(kableExtra)

formatted_table <- function(df) {
  df %>% kable() %>%
 kable_styling(bootstrap_options = c("striped", "hover", "condensed", "responsive"), full_width = F)
}


```

# Randomisation for the first bulb (reflector)

```{r randomisation_1}
all_treatment_color <- cross_df(
  tibble(color = c("nocolor", "red", "black", "green", "yellow"),
        treatment = c("water", "lowsugar", "highsugar", "lowsweetener", "highsweetener"))) %>% 
  mutate(id = seq(nrow(.)))

set.seed(1234)
all_treatment_color %>% sample_frac(1) %>% formatted_table()
```

# Randomisation for the second bulb (normal)

```{r randomisation_2}
set.seed(4321)
all_treatment_color %>% sample_frac(1) %>% formatted_table()
```

```{r scrape_spectrum_information}
if (file.exists('data/observations.rds')) {
  observations <- read_rds('data/observations.rds')
} else {
  observations <- list()
}
# Crawl spectralworkbench website to get observation names and user ids
for (id in rev(seq(169931, 170950))) {
  if (!id %in% names(observations)) {
    print(glue("Spectrum id: {id}"))
    rspns <- try(GET(glue("https://spectralworkbench.org/spectrums/{id}"),
                 timeout(10)))
    if (class(rspns) != "try-error") {
      web_page <- content(rspns)
      obs_name <- web_page %>%
        html_node("#graphing > div.row-fluid.datum-spectrum.header > div > h2 > a") %>%
        html_text() %>% trimws()
      user_id <- web_page %>%
        html_node("#graphing > div.row-fluid.datum-spectrum.header > div > div > div > p > a") %>%
        html_text()
      observations[[as.character(id)]] <-
        tibble(
          spectrum_id = id,
          user_id = user_id,
          obs_name = obs_name
        )
    }
    write_rds(observations, 'data/observations.rds')
  }

}
```
# Observation data

```{r filter_split_obs_data}
obs_data <- bind_rows(observations) %>% filter(user_id == "stat0029experiment") %>% 
  filter(str_count(obs_name, "_") >= 2) %>%
  # Split into two led types
  mutate(led_type = if_else(str_detect(obs_name, "^led2_"), "normal", "reflector")) %>% 
  mutate(obs_name = str_remove(obs_name, "^led2_")) %>% 
  # Split names into color, treatment and snap number
  separate(obs_name, into = c('color', 'treatment', 'num'), sep = "_") %>% 
  # Remove duplicates
  distinct(color, treatment, led_type, num, .keep_all = T)

obs_data %>% head(20) %>% formatted_table()
```
# Spectrum data

```{r download_and_load_spectra_jsons}
spectra <- list()
for (id in obs_data$spectrum_id) {
  filename <- paste0("data/", id, ".json")
  if (!file.exists(filename)) {
    dwnld_rspns <- try(GET(glue("https://spectralworkbench.org/spectrums/latest/{id}.json"),
                      write_disk(filename, overwrite = F),
                      timeout(10)))
  }
  data <- jsonlite::fromJSON(read_lines(filename))  
  if ("spectrum_id" %in% names(data)) {
  spectra[[id]] <- data$data$lines %>% mutate(spectrum_id = data$spectrum_id)
  }
}
spectra_data <- bind_rows(spectra)
spectra_data %>% head(20) %>% formatted_table()
```

# Grouping

```{r avg_in_wavelength_grp}
# Size of wavelength buckets
grp_size <- 10
spectra_grp <- spectra_data %>%
  # Split into groups by wavelength
  mutate(wavelength_grp = as.character(wavelength %/% grp_size * grp_size + grp_size)) %>% 
  # Group and compute mean intensity in group
  group_by(wavelength_grp, spectrum_id) %>% summarise(intensity = mean(average), grp_n = n()) %>%
   ungroup() %>% mutate(grp_n_median = median(grp_n)) %>% 
  # Remove groups that have too few observations in them
  filter(grp_n > 0.5 * grp_n_median) %>% 
  # Join information about spectrum
  inner_join(obs_data, by = "spectrum_id") 
```

```{r top_one_in_wavelength_grp}
spectra_top_one <- spectra_data %>%
  # Split into groups by wavelength
  mutate(wavelength_grp = as.character(wavelength %/% grp_size * grp_size + grp_size)) %>% 
  # Group and select top 1 wavelength
  group_by(wavelength_grp, spectrum_id) %>% top_n(1, wavelength) %>% 
  #summarise(intensity = mean(average), grp_n = n()) %>%
   ungroup() %>% mutate(intensity = average) %>% 
  # Join information about spectrum
  inner_join(obs_data, by = "spectrum_id") 
```


**`r glue("The group size is {grp_size}nm and the median number of pixels in the wavelength groups is {spectra_grp$grp_n_median[1]}.")`**

# Average in groups {.tabset .tabset-fade}

## ANOVA

```{r}
anova_all <- spectra_grp %>% aov(log1p(intensity)~color+treatment+wavelength_grp + led_type, .) 
anova_all %>% tidy() %>% formatted_table()
```
## Pairwise comparison

```{r}
TukeyHSD(anova_all, "treatment") %>% tidy() %>% formatted_table()
```

# Average in groups - only reflector {.tabset .tabset-fade}

Clearly reflector makes a huge difference. 

## ANOVA

```{r}
anova_reflector <- spectra_grp %>% filter(led_type == "reflector") %>% 
  aov(log1p(intensity)~color+treatment+wavelength_grp, .) 
anova_reflector %>% tidy() %>% formatted_table()
```

## Pairwise comparison

```{r}
TukeyHSD(anova_reflector, "treatment") %>% tidy() %>% formatted_table()
```

# Top 1 in groups {.tabset .tabset-fade}

## ANOVA

```{r}
anova_all_top_one <- spectra_top_one %>% aov(log1p(intensity)~color+treatment+wavelength_grp + led_type, .) 
anova_all_top_one %>% tidy() %>% formatted_table()
```
## Pairwise comparison

```{r}
TukeyHSD(anova_all_top_one, "treatment") %>% tidy() %>% formatted_table()
```

# Top 1 in groups - only reflector {.tabset .tabset-fade}

Clearly reflector makes a huge difference. 

## ANOVA

```{r}
anova_reflector_top_one <- spectra_top_one %>% filter(led_type == "reflector") %>% 
  aov(log1p(intensity)~color+treatment+wavelength_grp, .) 
anova_reflector_top_one %>% tidy() %>% formatted_table()
```

## Pairwise comparison

```{r}
TukeyHSD(anova_reflector_top_one, "treatment") %>% tidy() %>% formatted_table()
```

Using 580nm only as in this document;
[Determination of Sugar as Glucose in a Soft Drink Using the LAMBDA PDA UV/Vis Spectrophotometer](https://www.perkinelmer.com/lab-solutions/resources/docs/APP-Determination-of-Sugar-as-Glucose-in-Soft-Drink-using-LAMBDA-465-012356A_01.pdf)
Not sure why a red part of the spectrum works better. It might be worth checking.
Here they use even infrared (>700nm):
[Identification & Differentiation of Multiple Sugars](https://keit.co.uk/irmadillo-sugars/)



# Results for both bulbs {.tabset .tabset-fade}

## 440-540nm

```{r fig.height=10, fig.width=10}
spectra_grp %>% filter(as.numeric(wavelength_grp) < 540) %>%
  ggplot() + geom_boxplot(aes(x=led_type, y=intensity, fill=treatment)) + 
  facet_grid(wavelength_grp~color, scales = "free_y") +
  theme(legend.position="bottom")
```

## 540-640nm

```{r fig.height=10, fig.width=10}
spectra_grp %>% filter(as.numeric(wavelength_grp) >= 540) %>%
  ggplot() + geom_boxplot(aes(x=led_type, y=intensity, fill=treatment)) + 
  facet_grid(wavelength_grp~color, scales = "free_y") +
  theme(legend.position="bottom")
```

# Results for reflector only
```{r fig.height=10, fig.width=10}
spectra_grp %>% filter(led_type == "reflector") %>%
  ggplot() + geom_boxplot(aes(x=color, y=intensity, fill=treatment)) + 
  facet_grid(wavelength_grp~., scales = "free_y") +
  theme(legend.position="bottom")
```



# Comments & additional analysis

- it might be worth thinking about which contrasts are the most relevant. I only picked a few. 
- it might be interesting to see if there are some colors that make contrasts insignificiant.
- it would be good to run more tests with both bulbs, not just always exclude normal one. 
- it would be good to investigate wavelength groups. This 580nm feels a bit arbitrary. Investigating the box plot can be helpful. We had overexposure for blue color i.e. for low values in nm (when using reflector). It might have caused some issues. 
- we might consider using contrasts (it can be done in R, it is shown in notes) rather than running AnoVa with filtered data.frame. Results will be the same, but just for the presentation purposes

**I really think that we have a lot of stuff....**

